# dm-oscshell

Osc message to shell command 

## Ports 

listen multicast 239.200.200.200 on port 50555

Broadcast to port 50556

## Route 

All target
```
/dm/command exec list [commmand]
/dm/command script reboot.sh
```
specific target
```
/dm/command/[hostname] exec list [commmand]
/dm/command/hostname script reboot.sh
```
```